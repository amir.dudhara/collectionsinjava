package com.antra.raghu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

class AntraStudent{  
	int rollno;  
	String name;  
	int age;  
	AntraStudent(int rollno,String name,int age){  
		this.rollno=rollno;  
		this.name=name;  
		this.age=age;  
	}  
}  
class AgeComparator implements Comparator<AntraStudent>{  
	public int compare(AntraStudent s1,AntraStudent s2){  
		if(s1.age==s2.age)  
			return 0;  
		else if(s1.age>s2.age)  
			return 1;  
		else  
			return -1;  
	}  
}  
class NameComparator implements Comparator<AntraStudent>{  
	public int compare(AntraStudent s1,AntraStudent s2){  
		return s1.name.compareTo(s2.name);  
	}  
} 
public class ComparatorTestEx {

	public static void main(String[] args) {
		ArrayList<AntraStudent> al=new ArrayList<>();  
		al.add(new AntraStudent(101,"Vijay",23));  
		al.add(new AntraStudent(106,"Ajay",27));  
		al.add(new AntraStudent(105,"Jai",21));  

		System.out.println("Sorting by Name");  

		Collections.sort(al,new NameComparator());  
		Iterator<AntraStudent> itr=al.iterator();  
		while(itr.hasNext()){  
			AntraStudent st = itr.next();  
			System.out.println(st.rollno+" "+st.name+" "+st.age);  
		}  

		System.out.println("Sorting by age");  

		Collections.sort(al,new AgeComparator());  
		Iterator<AntraStudent> itr2=al.iterator();  
		while(itr2.hasNext()){  
			AntraStudent st=(AntraStudent)itr2.next();  
			System.out.println(st.rollno+" "+st.name+" "+st.age);  
		}  


	}  
}
