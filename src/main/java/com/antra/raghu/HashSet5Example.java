package com.antra.raghu;

import java.util.HashSet;

class ProgramBook {  
	int id;  
	String name;
	String author;
	String publisher;  
	int quantity;  
	public ProgramBook(int id, String name, String author, 
			String publisher, int quantity) {  
		this.id = id;  
		this.name = name;  
		this.author = author;  
		this.publisher = publisher;  
		this.quantity = quantity;  
	}  
} 
public class HashSet5Example {
	public static void main(String[] args) {  
		HashSet<ProgramBook> set=new HashSet<>();  
		//Creating Books  
		ProgramBook b1=new ProgramBook(101,"Let us C","Yashwant Kanetkar","BPB",8);  
		ProgramBook b2=new ProgramBook(102,"Data Communications & Networking","Forouzan","Mc Graw Hill",4);  
		ProgramBook b3=new ProgramBook(103,"Operating System","Galvin","Wiley",6);  
		//Adding Books to HashSet  
		set.add(b1);  
		set.add(b2);  
		set.add(b3);  
		//Traversing HashSet  
		for(ProgramBook b:set){  
			System.out.println(b.id+" "+b.name+" "+b.author+" "+b.publisher+" "+b.quantity);  
		}  
	}  
}
